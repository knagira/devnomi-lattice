test("test1", function() {
  // (0,0) (5,0) (0,5)
  var x1 = 0;
  var y1 = 5;
  var x2 = 5;
  var y2 = 0;
  var result = eval_lattice(x1,y1,x2,y2);
  equal(result, 6);
});

test("test2", function() {
  // (0,0) (5,5) (-5,5)
  var x1 = 5;
  var y1 = 5;
  var x2 = -5;
  var y2 = 5;
  var result = eval_lattice(x1,y1,x2,y2);
  equal(result, 16);
});

test("test3", function() {
  // (0,0) (-5,0) (5,0)
  var x1 = -5;
  var y1 = 0;
  var x2 = 5;
  var y2 = 0;
  var result = eval_lattice(x1,y1,x2,y2);
  equal(result, 0);
});

test("test4", function() {
  // (0,0) (0,5) (0,-5)
  var x1 = 0;
  var y1 = 5;
  var x2 = 0;
  var y2 = -5;
  var result = eval_lattice(x1,y1,x2,y2);
  equal(result, 0);
});

test("test5", function() {
  // (0,0) (1,50) (0,30)
  var x1 = 1;
  var y1 = 50;
  var x2 = 0;
  var y2 = 30;
  var result = eval_lattice(x1,y1,x2,y2);
  equal(result, 0);
});

test("test6", function() {
  // (0,0) (-2,2) (-5,-4)
  var x1 = -2;
  var y1 = 2;
  var x2 = -5;
  var y2 = -4;
  var result = eval_lattice(x1,y1,x2,y2);
  equal(result, 7);
});

test("test7", function() {
  // (0,0) (2,2) (-5,-2)
  var x1 = 2;
  var y1 = 2;
  var x2 = -5;
  var y2 = -2;
  var result = eval_lattice(x1,y1,x2,y2);
  equal(result, 2);
});

test("test8", function() {
  // (0,0) (2,8) (2,8)
  var x1 = 2;
  var y1 = 8;
  var x2 = 2;
  var y2 = 8;
  var result = eval_lattice(x1,y1,x2,y2);
  equal(result, 0);
});

test("test9", function() {
  // (0,0) (0,0) (0,0)
  var x1 = 0;
  var y1 = 0;
  var x2 = 0;
  var y2 = 0;
  var result = eval_lattice(x1,y1,x2,y2);
  equal(result, 0);
});

test("test10", function() {
  // (0,0) (30,21) (-10,-7)
  var x1 = 30;
  var y1 = 21
  var x2 = -10;
  var y2 = -7;
  var result = eval_lattice(x1,y1,x2,y2);
  equal(result, 0);
});

test("test11", function() {
  // (0,0) (0,0) (-10,-7)
  var x1 = 0;
  var y1 = 0;
  var x2 = -10;
  var y2 = -7;
  var result = eval_lattice(x1,y1,x2,y2);
  equal(result, 0);
});